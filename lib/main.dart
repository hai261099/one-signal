import 'package:flutter/material.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

void main() async {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  @override
  void initState() {
    super.initState();
    getToken();
  }

  void getToken() {
    _firebaseMessaging.getToken().then((token) {
      print("Token " + token);
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String notifyContent = '';

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    configOneSignal();
  }

  void configOneSignal() async {
    await OneSignal.shared.init("7b2461b1-c18d-4d78-9875-a58c42971119");

    ///show notification content
    OneSignal.shared
        .setInFocusDisplayType(OSNotificationDisplayType.notification);

    ///will be called whenever a notification is received
    OneSignal.shared.setNotificationReceivedHandler((notification) {
      ///content notification
      setState(() {
        notifyContent =
            notification.jsonRepresentation().replaceAll('\\n', '\n');
      });
    });

    ///will be called whenever a notification is opened/button pressed
    OneSignal.shared.setNotificationOpenedHandler((openedResult) {});

    ///will be called whenever the subscription changes
    OneSignal.shared.setSubscriptionObserver((changes) {});

    ///will be called whenever then user's email subscription changes
    OneSignal.shared.setEmailSubscriptionObserver((changes) {});

    ///ask user to push them notification
    OneSignal.shared.promptUserForPushNotificationPermission();

    OneSignal.shared.getPermissionSubscriptionState().then((value) {
      print(value.subscriptionStatus.userId);
      print("Token: " + value.subscriptionStatus.pushToken);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(child: Text(notifyContent)),
    );
  }
}
